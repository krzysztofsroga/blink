

## Błyski
Pliki oznaczone jako 'BLINK' to nagrania twarzy użytkownika w czasie gdy na ekranie przedstawiana jest krótka animacja, która powinna pokazać się w odbiciach w okularach lub oczach. Różnice w oświetleniu mogą też być widoczne na reszcie twarzy  lub w wypadku ataku - widoczne odbicia w monitorze. 

Pliki generowane przez aplikację mają następującą nazwę:

`{DATA}{GODZINA}BLINK{NUMER_ANIMACJI}.mp4`

gdzie `NUMER_ANIMACJI` jest cyfrą od 1 do 6. Do stworzenia plików zostało użytych 6 różnych 'efektów':

.|.
:-:|:-:
![BLINK1](blink1.mp4) | ![BLINK2](blink2.mp4) 
![BLINK3](blink3.mp4) | ![BLINK4](blink4.mp4)
![BLINK5](blink5.mp4) | ![BLINK6](blink6.mp4)

Nagranie BLINK2 czasami odtwarza się dość dziwnie na tej stronie. Prawdopodobnie jest problem z synchronizacją ekranu

Każda z animacji ma 32 klatki, czyli powinna trwać około pół sekundy. 

`BLINK1` oraz `BLINK2` obraca pokazany obraz co każdą wyświetlaną klatkę.

`BLINK3` oraz `BLINK4` obraca pokazany obraz co czwartą wyświetlaną klatkę.

`BLINK5` oraz `BLINK5` obraca pokazany obraz co drugą wyświetlaną klatkę.

Są także nagrania z dopiskiem `CLEAN` - są one głównie do celów porównawczych. Są to nagrania podczas których nie został użyty żaden efekt, ale zostały nagrane tą samą aplikacją. 
